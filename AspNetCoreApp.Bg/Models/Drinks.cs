﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using AspNetCoreApp.Bg.Models.Attr;

namespace AspNetCoreApp.Bg.Models
{
    public class Drinks : BaseEntity
    {
        public string Name { get; set; }
        [Column(TypeName = "numeric")]
        [DecimalPrecision(10, 2)]
        public decimal Price { get; set; }
        public DateTime ExpirationTime { get; set; }
        public string NetContent { get; set; }
    }
}
