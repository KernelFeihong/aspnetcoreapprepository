﻿using System;

namespace AspNetCoreApp.Bg.Models.Attr
{
    /// <summary>
    /// <para>自定义Decimal类型的精度属性</para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class DecimalPrecisionAttribute : Attribute
    {
        #region Field

        private byte _precision = 18;
        public byte _scale = 0;

        #endregion Field

        #region Construct

        /// <summary>
        /// <para>自定义Decimal类型的精确度属性</para>
        /// </summary>
        /// <param name="precision">precision
        /// <para>精度（默认18）</para></param>
        /// <param name="scale">scale
        /// <para>小数位数（默认0）</para></param>
        public DecimalPrecisionAttribute(byte precision = 18, byte scale = 0)
        {
            Precision = precision;
            Scale = scale;
        }

        #endregion Construct

        #region Property

        /// <summary>
        /// 精确度（默认18）
        /// </summary>
        public byte Precision
        {
            get { return _precision; }
            set { _precision = value; }
        }

        /// <summary>
        /// 保留位数（默认0）
        /// </summary>
        public byte Scale
        {
            get { return _scale; }
            set { _scale = value; }
        }

        #endregion Property
    }
}