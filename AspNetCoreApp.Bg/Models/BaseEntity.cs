﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCoreApp.Bg.Models
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
        public Guid CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public Guid LastModifyBy { get; set; }
        public DateTime? LastModifyTime { get; set; }
    }
}
