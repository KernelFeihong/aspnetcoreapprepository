﻿using AspNetCoreApp.Bg.IRepository;
using AspNetCoreApp.Bg.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCoreApp.Bg.Controllers
{
    [Route("Test")]
    public class TestController : Controller
    {
        private readonly ITestRepository _repository;
        public TestController(ITestRepository repository)
        {
            _repository = repository;
        }

        [Route("Index")]
        public ActionResult Index()
        {
            return View();
        }

        [Route("GetDrinks")]
        public ActionResult<List<Drinks>> GetDrinks()
        {
            var result = _repository.GetDrinks().ToList();
            return Json(result);
        }
    }
}
