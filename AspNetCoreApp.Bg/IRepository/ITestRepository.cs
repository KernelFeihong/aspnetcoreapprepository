﻿using AspNetCoreApp.Bg.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCoreApp.Bg.IRepository
{
    public interface ITestRepository
    {
        IEnumerable<Drinks> GetDrinks();
        Drinks GetSingleDrink(Guid drinkId);
    }
}
