﻿using AspNetCoreApp.Bg.IRepository;
using AspNetCoreApp.Bg.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCoreApp.Bg.Repository
{
    public class TestRepository : ITestRepository
    {
        private readonly List<Drinks> _drinks;

        public TestRepository()
        {
            if (_drinks == null) 
            {
                _drinks = InitDrinks();
            }
        }

        public List<Drinks> InitDrinks()
        {
            return new List<Drinks>()
            {
                new Drinks(){ Id=Guid.NewGuid(), Name="可乐",Price=18.50M,ExpirationTime=DateTime.Now.AddMonths(2), NetContent="330毫升",CreateBy=Guid.NewGuid(),CreateTime=DateTime.Now,LastModifyBy=Guid.NewGuid(),LastModifyTime=DateTime.Now },
                new Drinks(){ Id=Guid.NewGuid(), Name="雪碧",Price=18.50M,ExpirationTime=DateTime.Now.AddMonths(4), NetContent="500毫升",CreateBy=Guid.NewGuid(),CreateTime=DateTime.Now,LastModifyBy=Guid.NewGuid(),LastModifyTime=DateTime.Now  },
                new Drinks(){ Id=Guid.NewGuid(), Name="牛奶",Price=18.50M,ExpirationTime=DateTime.Now.AddMonths(5), NetContent="800毫升",CreateBy=Guid.NewGuid(),CreateTime=DateTime.Now,LastModifyBy=Guid.NewGuid(),LastModifyTime=DateTime.Now  }
            };
        }

        public IEnumerable<Drinks> GetDrinks()
        {
            return _drinks;
        }

        public Drinks GetSingleDrink(Guid drinkId)
        {
            return _drinks.FirstOrDefault(s => s.Id == drinkId);
        }
    }
}
